module.exports = function (req, res, next) {
    if (req.loggedInUser.role !== 1) {
        next({
            msg: 'you dont have permision'
        })
    } else {
        return next();
    }
}