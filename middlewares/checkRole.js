module.exports = function (req, res, next) {
    if (req.query.role == 1) {
        return next();
    }
    else {
        next({
            status: 403,
            msg: 'you dont have permission'
        })
    }
}