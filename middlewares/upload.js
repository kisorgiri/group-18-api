var multer = require('multer');
// simple usuage
// var upload = multer({
//     dest: './uploads/'
// })
var myStorage = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, new Date().getTime() + '-' + file.originalname);
    },
    destination: function (req, file, cb) {
        cb(null, './uploads/images/')
    }
});

function fileFilter(req, file, cb) {
    var mimeType = file.mimetype.split('/')[0];
    if (mimeType == 'image') {
        cb(null, true);
    } else {
        req.fileError = true;
        cb(null, false);
    }
}

var upload = multer({
    storage: myStorage,
    fileFilter: fileFilter
});

module.exports = upload;