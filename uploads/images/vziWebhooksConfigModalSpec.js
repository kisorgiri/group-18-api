import { mockWebhook, mockWebhookEntity, mockEventSubscriptionData, mockEventSubscriptionDataModels } from 'fixtures/webhooks';
import { MockPromise } from 'spec_helper';

describe('Component: vziWebhooksConfigModal', () => {
    let $componentController, $ctrl, $q, $scope;
    let NotificationService, WebhooksService;
    let modalInstance, saveDefered;

    beforeEach(() => {
        angular.mock.module('adminApp');

        inject($injector => {
            $componentController = $injector.get('$componentController');
            $q = $injector.get('$q');
            $scope = $injector.get('$rootScope').$new();
            WebhooksService = $injector.get('WebhooksService');
            NotificationService = $injector.get('NotificationService');

            modalInstance = {
                dismiss: jasmine.createSpy('modalInstance.dismiss'),
                close: jasmine.createSpy('modalInstance.close'),
            };
            saveDefered = $q.defer();
            spyOn(WebhooksService, 'add').and.callFake(MockPromise({ data: { ...mockWebhook }, success: true }, saveDefered.promise));
            spyOn(WebhooksService, 'update').and.callFake(MockPromise({ data: { ...mockWebhook }, success: true }, saveDefered.promise));
            spyOn(NotificationService, 'displayError');
            spyOn(NotificationService, 'displaySuccess');
        });
    });

    afterEach(() => $ctrl = null);

    function setupController(locals = { $scope }, bindings = {
        formData: {},
        modalInstance,
    }) {
        return $componentController('vziWebhooksConfigModal', locals, bindings);
    }

    it('should be defined', () => {
        $ctrl = setupController();

        expect($ctrl).toBeDefined();
    });

    describe('method: $onInit', () => {
        beforeEach(() => {
            $ctrl = setupController();
            $ctrl.$onInit();
        });

        it('should initialize active option values', () => {
            expect($ctrl.activeOptions).toBeDefined();
            expect($ctrl.activeOptions.length).toBe(2);
            expect($ctrl.activeOptions[0].value).toBe(true);
        });

        it('should initialize webhooks data binding', () => {
            expect($ctrl.formData).toBeDefined();
        });

        it('should set modal title', () => {
            expect($ctrl.title).toBe('page.webhooks.webhooks_configuration.add.title');
        });

        it('should fetch event subscription list for entities and actions', () => {
            expect($ctrl._fetchEventSubscriptionList).toHaveBeenCalled();
        });
    });

    describe('method: cancel', () => {
        it('should dismiss the modal', () => {
            $ctrl = setupController();

            $ctrl.cancel();

            expect(modalInstance.dismiss).toHaveBeenCalled();
        });
    });

    describe('method: _filteredEvents', () => {
        beforeEach(() => {
            $ctrl = setupController();
        });

        it('should prepare selected data to be sent to BE', () => {
            //when
            const eventSubscriptions = $ctrl._filteredEvents(angular.copy(mockEventSubscriptionDataModels));

            expect(eventSubscriptions).toBe(mockEventSubscriptionData);
        });
    });

    describe('method: selectAction', () => {
        beforeEach(() => {
            $ctrl = setupController();
        });

        it('should set $$selected flag true for entites when action is checked', () => {
            //when
            $ctrl.selectAction(mockWebhookEntity);

            expect(mockWebhookEntity.$$selected).toBe(true);
        });
    });

    describe('method: save', () => {
        describe('with new webhook config', () => {
            beforeEach(() => {
                $ctrl = setupController();

                $ctrl.save();
            });

            it('should set isSaving flag to true', () => {
                expect($ctrl.isSaving).toBe(true);
            });

            it('should call service to add new webhook config', () => {
                expect(WebhooksService.add).toHaveBeenCalledWith({});
            });

            describe('on success', () => {
                beforeEach(() => {
                    saveDefered.resolve();
                    $scope.$digest();
                });

                it('should display success notification', () => {
                    expect(NotificationService.displaySuccess).toHaveBeenCalledWith('page.webhooks.webhooks_configuratio.add.success_text');
                });

                it('should close modal', () => {
                    expect(modalInstance.close).toHaveBeenCalledWith(mockWebhook);
                });

                it('should set isSaving flag to false', () => {
                    expect($ctrl.isSaving).toBe(false);
                });
            });

            describe('on failure', () => {
                beforeEach(() => {
                    saveDefered.reject();
                    $scope.$digest();
                });

                it('should display error notification', () => {
                    expect(NotificationService.displayError).toHaveBeenCalled();
                });

                it('should not close modal', () => {
                    expect(modalInstance.close).not.toHaveBeenCalled();
                    expect(modalInstance.dismiss).not.toHaveBeenCalled();
                });

                it('should set isSaving flag to false', () => {
                    expect($ctrl.isSaving).toBe(false);
                });
            });
        });

        describe('with existing webhook', () => {
            beforeEach(() => {
                $ctrl = setupController();
                $ctrl.formData = { ...mockWebhook };

                $ctrl.save();
            });

            it('should set isSaving flag to true', () => {
                expect($ctrl.isSaving).toBe(true);
            });

            it('should call service to update webhook', () => {
                expect(WebhooksService.update).toHaveBeenCalledWith({ ...mockWebhook });
            });
            describe('on success', () => {
                beforeEach(() => {
                    saveDefered.resolve();
                    $scope.$digest();
                });

                it('should display success notification', () => {
                    expect(NotificationService.displaySuccess).toHaveBeenCalledWith('page.webhooks.webhooks_configuratio.update.success_text');
                });

                it('should close modal', () => {
                    expect(modalInstance.close).toHaveBeenCalledWith(mockWebhook);
                });

                it('should set isSaving flag to false', () => {
                    expect($ctrl.isSaving).toBe(false);
                });
            });

            describe('on failure', () => {
                beforeEach(() => {
                    saveDefered.reject();
                    $scope.$digest();
                });

                it('should display error notification', () => {
                    expect(NotificationService.displayError).toHaveBeenCalled();
                });

                it('should not close modal', () => {
                    expect(modalInstance.close).not.toHaveBeenCalled();
                    expect(modalInstance.dismiss).not.toHaveBeenCalled();
                });

                it('should set isSaving flag to false', () => {
                    expect($ctrl.isSaving).toBe(false);
                });
            });
        });
    });
});
