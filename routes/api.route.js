const router = require('express').Router();
// load middleware
const authenticate = require('./../middlewares/authenticate');
const authorize = require('./../middlewares/authorize');

// load subrouting files
const productRouter = require('./../components/products/routes/product.route');
const authRouter = require('./../controllers/auth.route');
const userRouter = require('./../controllers/user.route');

router.use('/product', productRouter);
router.use('/user', authenticate, authorize, userRouter);
router.use('/auth', authRouter);

module.exports = router;