const express = require('express')
const fileOp = require('./file');
const app = express(); // app is entire express framework
const port = 9090;
const morgan = require('morgan');
const path = require('path');
app.set('port', 8080);
const cors = require('cors');

const apiRoute = require('./routes/api.route');

// calling another file expecting nothing
require('./db');
// applicaiton level middleware


// events
var ev = require('events');
const event = new ev.EventEmitter();
app.use(function (req, res, next) {
    req.myEvent = event;
    next();
})
// socket stuff
const socket = require('socket.io');
var io = socket(app.listen(8081));
io.on('connection', function (client) {
    console.log('client connected to socket server');
    client.on('hi', function (data) {
        console.log("data in hi >>", data);
        client.emit('hello', 'Hello from server');
    })
    client.on('new-msg', function (data) {
        client.emit('reply-msg', data); // aafno lagi matrai
        // client.broadcast.emit('reply-msg',data); // brodcast will send the event to all other connected client except own client
    })
});

// load third party middleware
app.use(morgan('dev'));

//load inbuilt middleware
// app.use(express.static('files')) // for internal use within express
app.use('/file', express.static(path.join(__dirname, 'uploads')));
// parse incoming template form urlencoded data
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());
// if data is in json we have to parse using json parse
// if data is in form-data(used for image uploading) we have to use form-data parser

app.use(cors()); // accept all request
// routing configuration
app.use('/api', apiRoute);

app.use(function (req, res, next) {
    // 404 error handler 
    next({
        msg: 'Not Found',
        status: 404
    })
})

event.on('product_data', function (data) {
    console.log('data .>', data);
})
event.on('errors', function (erros) {
    console.log('application errors >>', erros);
})
// error handling middleware
app.use(function (err, req, res, next) {
    console.log('i am error handling middleware', err);
    res.status(err.status || 400).json({
        status: err.status || 400,
        msg: err.msg || err,
    });
});

app.listen(app.get('port'), function (err, done) {
    if (err) {
        console.log('server listening failed');
    } else {
        console.log('server listening at port ' + app.get('port'));
        console.log('press CTRL + C to exit');
    }
});

// middlewares
// middleware is function that has access to http request object
// http response object and next middleware function refrence
// middleware always came into action in request response cycle
// middleware can modify the http request object and response object
// middeware are very powerful

// types of middlewares
// application level middleware
// hamro code base ma req res and next ko scope vako middle is application level middleware
// routing level middleware
// error handling middleware
// third party middleware // middleware that are from npmjs
// inbuilt middleware



// express and framework
// nodemon monitoring tool
// express way of handling request
// add individual handler for each and every request
// request
    // dynmaic url with value
    // req.params
    // optional url value
    // req.query
// response
// response status ,sendStatus send , end, json
// 2 choti response janu vayen gayo vane error aauch (can't set headers after they are sent)
// middleware
// functtion that has access to http request object and response object
// app.use() configuration block for middleware
// the order of middleware matters
// thirdparty middleware (npmjs ma vako middleware)