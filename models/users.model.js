const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: String,
    email: {
        type: String,
        unique: true,
    },
    phoneNumber: {
        type: Number
    },
    address: {
        type: String
    },
    role: {
        type: Number, // 1,2,3 1 admin, 2 normal user 3 visitors
        default: 2
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['online', 'offline', 'away'],
        default: 'online'
    },
    dob: Date,
    gender: {
        type: String
    },
    image: String,
    country: String,
    passwordResetToken: String,
    passwordResetTokenExpiry: Date
}, {
    timestamps: true
});

const user = mongoose.model('user', userSchema);
module.exports = user;