// distributed database management system

// mongodb
// shell command
// mongo ===> this command will connect to mongo server and give you interface to work with mongodb database
// proceed only if arrow head is available
//show dbs // show all the available databases
// use <db_name> if (db_name exist it will select existing db or create new one);
// db ==> show selected db
// show collections shows collection of selected db

// to insert or create new collection
// db.<coll_name>.insert({valid_json});
// it will give us _id property having ObjectId
// recommended not to modify or overide _id property

// db.<coll_name>.find({query builder}) //empty query bata kaam gardai gara
// db.<coll_name>.find().pretty() // pretty the content
// db.<coll_name>.count() // return number documents

// update from shell command
// use  <db_name>
// // db.<coll_name>.update({query_builder},{payload},{otions})
// payload ==> {
//     $set:{actual data to be updated}
// }

// remove
// db.<coll_name>.remove({query builder});// not try to remove with empty query

// drop collection
// db.<coll_name>.drop()
// drop database
// db.dropDatabse();

// #######################BACKUP & RESTORE #################
// mongorestore mongodump mongoimport mongoexport
// back and restore can be done in two format
// bson format (binay json) and human readable format
// bson format
// Backup
// command
// mongodump ==> it will backup all the databse into default dump folder
// mongodump --db <db_name> it will  back up selected databse in default dump folder
// mongodump --db <db_name> --out <output_directory_path>

// restore 
// mongorestore  ==>it will search for default dump folder and then tries to backup all the databse existing in dump
// mongorestore path_to_backedup_folder ==> restore from other then dump folder

// human readable format(json and csv)
// backup
// mongoexport 
// json
// mongoexport --db <db_name> --collection <collection_name>  --out <outPut directory with .json extension)
// mongoexport -d <db_name> -c <collection_name>  -o <outPut directory with .json extension)

// restore command
// mongoimport 
// mongoimport --db <db_name> --collection <col_name> path to json file

// csv format
// backup
// mongoexport --db <db_name> --collection <col_name> --type=csv --fields 'comma seperated property name to be exported' --query="{class:7}" --out <output dierectory with .csv extension>
// #######################BACKUP & RESTORE #################

// import form csv
// mongoimport --db <db_name> --collection <col_name> --type=csv location of csv file --headerline
// mongoexport and mongoimport always cames in pair
// mongodump and mongorestore always came in pair

// 
