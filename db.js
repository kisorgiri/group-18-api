// ODM Object Document Modeling
// databaase as an programmaing language object jasari chalauna lai

// advantanges
// Schema based solution
// data types
// methods 
// middlewares
// plugins (helpful)
// easy indexing

const mongoose = require('mongoose');
const config = require('./configs/db.config');


mongoose.connect(config.conxnURL + "/" + config.dbName, { useNewUrlParser: true });
mongoose.connection.once('open', function () {
    console.log('db connection success');
})
mongoose.connection.on('err', function (err) {
    console.log('db connection faled,>>', err);
})