const router = require('express').Router();
const UserModel = require('./../models/users.model');
const map_user_req = require('./../helpers/map_user_request');
router.route('/')
    .get(function (req, res, next) {
        // console.log('root path ?>>', process.cwd());
        // console.log('directory path >>', __dirname);
        UserModel
            .find({})
            // .sort({
            //     _id: -1
            // })
            // .limit(2)
            // .skip(1)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(users);
            })
    })
    .post(function (req, res, next) {

    });

router.route('/profile')
    .get(function (req, res, next) {

    })
    .post(function (req, res, next) {
        console.log('i came from form');
        res.json({
            hi: 'hello from user post request'
        })
    })
    .put(function (req, res, next) {

    })
    .delete(function (req, res, next) {

    });

router.route('/change-password')
    .get(function (req, res, next) {

    })
    .post(function (req, res, next) {

    })
    .put(function (req, res, next) {

    })
    .delete(function (req, res, next) {

    });

router.route('/:id')
    .get(function (req, res, next) {
        var id = req.params.id;
        UserModel.findById(id, function (err, user) {
            if (err) {
                return next(err);
            }
            res.status(200).json(user);
        })
        // user find  by id
    })
    .put(function (req, res, next) {
        // find by id and update
        const id = req.params.id;
        UserModel.findOne({ _id: id })
            .exec(function (err, user) {
                if (err) {
                    return next(err);
                }
                if (user) {
                    var updatedMappedUser = map_user_req(user, req.body);
                    updatedMappedUser.save(function (err, updated) {
                        if (err) {
                            return next(err);
                        }
                        res.json(updated);
                    })

                } else {
                    next({
                        msg: 'user not found',
                    })
                }
            })

    })
    .delete(function (req, res, next) {
        // delete by id
        var id = req.params.id;
        UserModel.findById(id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                user.remove(function (err, removed) {
                    if (err) {
                        return next(err);
                    }
                    res.status(200).json(removed);
                });
            } else {
                res.json({
                    msg: "user not found"
                })
            }
        })

    });

module.exports = router;