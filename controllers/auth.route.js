const express = require('express');
const router = express.Router();
const UserModel = require('./../models/users.model');
const mapUser = require('./../helpers/map_user_request');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../configs');
const nodemailer = require('nodemailer');
const randomString = require('randomstring');

const sender = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'broadwaytest44@gmail.com',
        pass: 'Broadwaytest44!'
    }
})

function perpareMail(data) {
    let mailBody = {
        from: 'Our web store <noreply@ourwebstore.com>', // sender address
        to: data.email + ',binodraazdhami@gmail.com,kmushyakhwa@gmail.com', // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `
        <p>Hi <strong>${data.name}</strong>,</p>
        <p>We noticed that you are having problem logging into our web store.please use the link below to reset your password
        please note that this link will work within a day only</p>
        <p><a href='${data.link}'>click here to reset</a></p>
        <p>If you didnot sent request to change your password kindly ignore this email.</p>
        <p>Regards,</p>
        <p>Our Web Store</p>
        ` // html body         
    }
    return mailBody;
}

router.post('/login', function (req, res, next) {
    console.log('i am here when form is submitted', req.body);
    // res.redirect('/auth/login');
    UserModel.findOne({
        $or: [
            {
                email: req.body.username
            },
            {
                username: req.body.username
            }
        ]
    })
        .then(function (user) {
            console.log('user >>>', user);
            if (user) {
                var isMatched = passwordHash.verify(req.body.password, user.password)
                if (isMatched) {
                    var token = jwt.sign({ id: user._id }, config.jwtSecret);
                    res.status(200).json({
                        user: user,
                        token: token
                    });
                }
                else {
                    next({
                        msg: 'invalid password'
                    })
                }
            } else {
                next({
                    msg: "invalid username"
                })
            }
        })
        .catch(function (err) {
            return next(err);
        })
})

router.get('/register', function (req, res, next) {

})
router.post('/register', function (req, res, next) {
    console.log('what comes in >>', req.body);
    // insert
    var newUser = new UserModel({});
    var newMappedUser = mapUser(newUser, req.body);
    newMappedUser.password = passwordHash.generate(req.body.password);
    newMappedUser.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.status(200).json(done);
    })
})

router.post('/forgot-password', function (req, res, next) {
    UserModel.findOne({
        email: req.body.email
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                // email sending
                const passwordResetToken = randomString.generate(25);
                const passwordResetTokenExpiry = new Date(Date.now() + 1000 * 60 * 60 * 24);


                var mailData = {
                    name: user.username,
                    email: user.email,
                    link: `${req.headers.origin}/reset-password/${passwordResetToken}`
                }
                const mailContent = perpareMail(mailData);

                user.passwordResetToken = passwordResetToken;
                user.passwordResetTokenExpiry = passwordResetTokenExpiry;
                user.save(function (err, saved) {
                    if (err) {
                        return next(err);
                    }
                    sender.sendMail(mailContent, function (err, done) {
                        if (err) {
                            return next(err);
                        }
                        res.json(done);
                    })
                });
            }
            else {
                next({
                    msg: 'Email not registered yet',
                    status: 404
                });
            }
        })
});

router.post('/reset-password/:token', function (req, res, next) {
    var token = req.params.token;
    UserModel.findOne({
        passwordResetToken: token,
        passwordResetTokenExpiry: {
            $lte: Date.now()
        }
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            console.log('user >>>', user);
            if (!user) {
                return next({
                    msg: "Password Reset Token Invalid or Expired"
                });
            }
            // var currentTime = Date.now();
            // var resetExpiryTime = new Date(user.passwordResetTokenExpiry).getTime();

            // if (currentTime > resetExpiryTime) {
            //     return next({
            //         msg: "Password reset Link expired"
            //     })
            // }

            user.password = passwordHash.generate(req.body.password);
            user.passwordResetToken = null;
            user.passwordResetTokenExpiry = null;

            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })

})

module.exports = router;