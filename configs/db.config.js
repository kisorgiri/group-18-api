
const mongodb = require('mongodb');
const mongoClient = mongodb.MongoClient;
const conxnURL = 'mongodb://localhost:27017';
const dbName = 'group18db';
const oid = mongodb.ObjectID;

module.exports = {
    mongoClient, conxnURL, dbName, oid

}