// client -server architecture
// where there will be two programme 
// one programme will act as an server
// one programme will act as an client
// a programme which initiates request is client
// a programme which accepts request and repond is server
const fileOp = require('./file');
const http = require('http');

const server  = http.createServer(function(request,response){
    // request || 1st argument http request object
    // response || 2nd argument http response object
    console.log('request >>',request.method);
    console.log('request url >>',request.url);
    console.log('client conneted to server');
    // response.end('hello from node server')
    if(request.url == '/write'){
            fileOp.write('node.txt','testing clien-server')
                .then(function(data){
                    response.end('file writing success');
                })
                .catch(function(err){
                    response.end('file writing failed');
                })
    }
    else if (request.url == '/read'){

    }
    else{
        response.end('no any action to perform');
    }
});

server.listen(9090,function(err,done){
    if(err){
        console.log('error listening',err);
    }else{
        console.log('server listenign at port 9090')
    }
});
