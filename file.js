
// import garda node_module and node js ko inbuilt module lai path dinu pardaina

const fs = require('fs');
// file system module used to work with files in your system
// fs.writeFile('./alkdfj/abcd.txt','welcome to abcd nodejs',function(err,done){
//         if(err){
//             console.log('error writing file',err);
//         }else{
//             console.log('file writing success',done);
//         }
// });

// read
// fs.readFile('kishors.txt','UTF-8',function(err,done){
//     if(err){
//         console.log('error reading file >>',err);
//     }
//     else{
//         console.log('result >>',done);
//     }
// })

// rename
// fs.rename('kishor.txt','new-kishor.txt',function(err,done){
//     if(err){
//         console.log('error renaming >>',err);
//     }
//     else{
//         console.log('renamed');
//     }
// })

// remove
// fs.unlink('new-kishor.txt',function(err,done){
//     if(err){
//         console.log('error removing',err);
//     }else{
//         console.log('removed',done);
//     }
// })

function write(name,content){
    return new Promise(function(resolve,reject){
        fs.writeFile(name,content,function(err,done){
            if(err){
                reject(err);
            }else{
                resolve(done);
            }
        });
    });
}

// write('node.js','abcd')
//     .then(function(data){
//         console.log('sucess in write >>',data);
//     })  
//     .catch(function(err){
//         console.log('error in write >>',err);
//     })

    module.exports = {
        write
    };