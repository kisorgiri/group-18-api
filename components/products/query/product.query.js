const ProductModel = require('./../models/product.model');

function map_product_request(product, proudctDetails) {
    // console.log('tags >>>', proudctDetails.tags);
    // console.log('check type >>', typeof (proudctDetails.tags));
    if (proudctDetails.name)
        product.name = proudctDetails.name;
    if (proudctDetails.description)
        product.description = proudctDetails.description;
    if (proudctDetails.brand)
        product.brand = proudctDetails.brand;
    if (proudctDetails.category)
        product.category = proudctDetails.category;
    if (proudctDetails.color)
        product.color = proudctDetails.color;
    if (proudctDetails.price)
        product.price = proudctDetails.price;
    if (proudctDetails.weight)
        product.weight = proudctDetails.weight;
    if (proudctDetails.manuDate)
        product.manuDate = proudctDetails.manuDate;
    if (proudctDetails.expiryDate)
        product.expiryDate = proudctDetails.expiryDate;
    if (proudctDetails.status)
        product.status = proudctDetails.status;
    if (proudctDetails.image)
        product.image = proudctDetails.image;
    if (proudctDetails.batchNo)
        product.batchNo = proudctDetails.batchNo;
    if (proudctDetails.tags) {
        // product.tags = Array.isArray(proudctDetails.tags)
        //     ? proudctDetails.tags
        //     : proudctDetails.tags.split(',');
        product.tags = typeof (proudctDetails.tags) === 'string'
            ? proudctDetails.tags.split(',')
            : proudctDetails.tags;
    }
    if (proudctDetails.quantity)
        product.quantity = proudctDetails.quantity;
    if (proudctDetails.user)
        product.user = proudctDetails.user;
    if (proudctDetails.discountedItem) {
        product.discount = {
            discountedItem: proudctDetails.discountedItem,
            discountType: proudctDetails.discountedItem == 'false' ? null : proudctDetails.discountType,
            discountValue: proudctDetails.discountedItem == 'false' ? null : proudctDetails.discountValue,
        }
    }

    return product;
}

function find(condition, options = {}) {
    var pageNumber = Number(options.pageNumber) || 1;
    var pageSize = Number(options.pageSize) || 100;
    var perPage = (pageNumber - 1) * pageSize;
    console.log('limit >>', pageSize);
    console.log('skip >>', perPage);
    return ProductModel
        .find(condition)
        .sort({ _id: -1 })
        .limit(pageSize)
        .skip(perPage)
        .exec();
}


function insert(data) {
    var newProduct = new ProductModel({});
    var newMappedProduct = map_product_request(newProduct, data);
    return newMappedProduct.save();

}

function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id)
            .exec(function (err, product) {
                if (err) {
                    return reject(err);
                }
                if (!product) {
                    return reject({
                        msg: 'product not found'
                    })
                }
                var updatedProduct = map_product_request(product, data);
                updatedProduct.save(function (err, updated) {
                    if (err) {
                        reject(err);
                    }
                    else {
                        // TODO remove exisiing image
                        resolve(updated);
                    }
                });
            });
    });
}
function remove(id) {
    return ProductModel.findByIdAndRemove(id);
}

module.exports = {
    find, insert, update, remove, map_product_request
}