var upload = require('./../../../middlewares/upload');
var productCtrl = require('./../controllers/product.ctrl');
var router = require('express').Router();
const authenticate = require('./../../../middlewares/authenticate');

router.route('/')
    .get(authenticate, productCtrl.findAll)
    .post(authenticate, upload.single('img'), productCtrl.insert)

router.route('/search')
    .get(productCtrl.searchByGet)
    .post(productCtrl.searchByPost);

router.route('/:id')
    .get(authenticate, productCtrl.getById)
    .put(authenticate, upload.single('img'), productCtrl.update)
    .delete(authenticate, productCtrl.remove);



module.exports = router;



