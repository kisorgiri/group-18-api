const productQuery = require('./../query/product.query');
const fs = require('fs');
const path = require('path');

function insert(req, res, next) {
    // req.body 
    // business logic

    console.log('req.file >>>', req.file);
    console.log('req.body >>', req.body);

    if (req.file) {
        var mimeType = req.file.mimetype.split('/')[0];
        if (mimeType != 'image') {
            fs.unlink(path.join(process.cwd(), 'uploads/images/' + req.file.filename), function (err, done) {
                if (err) {
                    console.log('file removing error');
                }
                else {
                    console.log('file removed');
                }
            });
            return next({
                msg: 'invalid file format'
            });
        }
    }
    if (req.fileError) {
        return next({
            msg: 'Invalid file format'
        })
    }

    if (req.file) {
        req.body.image = req.file.filename;
    }
    req.body.user = req.loggedInUser.id;
    productQuery.insert(req.body)
        .then(function (data) {
            req.myEvent.emit('product_data', data);
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });

}

function findAll(req, res, next) {
    // console.log('req.proeprty >>', req.loggedInUser);
    // business logic 
    var condition = {};
    if (req.loggedInUser.role != 1) {
        condition.user = req.loggedInUser.id;
    }
    productQuery.find(condition)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function getById(req, res, next) {
    var id = req.params.id;
    var condition = { _id: id };
    productQuery.find(condition)
        .then(function (data) {
            res.status(200).json(data[0]);
        })
        .catch(function (err) {
            next(err);
        })
}

function searchByGet(req, res, next) {
    var condition = {};
    // var searchCondition = productQuery.map_product_request(condition, req.query);
    // console.log('search condition >>', searchCondition);
    // productQuery.find(searchCondition)
    //     .then(function (data) {
    //         res.status(200).json(data);
    //     })
    //     .catch(function (err) {
    //         next(err);
    //     })

    require('fs').readFile('sdklfj', function (err, done) {
        if (err) {
            req.myEvent.emit('errors', err);
        }
    })
}
function searchByPost(req, res, next) {
    var condition = {};
    var searchCondition = productQuery.map_product_request(condition, req.body);
    if (req.body.minPrice) {
        searchCondition.price = {
            $gte: req.body.minPrice
        }
    }
    if (req.body.maxPrice) {
        searchCondition.price = {
            $lte: req.body.maxPrice
        }
    }
    if (req.body.minPrice && req.body.maxPrice) {
        searchCondition.price = {
            $gte: req.body.minPrice,
            $lte: req.body.maxPrice
        }
    }

    if (req.body.fromDate) {
        var toDate = req.body.toDate
            ? req.body.toDate
            : req.body.fromDate;
        var fromDateTime = new Date(req.body.fromDate).setHours(0, 0, 0, 0);
        var toDateTime = new Date(toDate).setHours(23, 59, 0, 0);

        searchCondition.createdAt = {
            $gte: new Date(fromDateTime),
            $lte: new Date(toDateTime)
        }
    }
    productQuery.find(searchCondition, req.query)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function remove(req, res, next) {
    productQuery
        .remove(req.params.id)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

function update(req, res, next) {
    console.log('update here <<<>>>', req.body);
    console.log('req.file >>>', req.file);

    if (req.fileError) {
        return next({
            msg: 'Invalid file format'
        })
    }

    if (req.file) {
        req.body.image = req.file.filename;
    }

    productQuery.update(req.params.id, req.body)
        .then(function (data) {
            // TODO remove exisiing image
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}


module.exports = {
    findAll, insert, getById, searchByGet, searchByPost, remove, update
}