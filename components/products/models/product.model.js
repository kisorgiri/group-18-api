var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var reviewSchmea = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    message: String,
    point: Number
}, {
        timestamps: true
    });

var productSchema = new Schema({
    name: String,
    brand: String,
    category: {
        type: String,
        required: true,
    },
    price: Number,
    color: String,
    batchNo: String,
    weight: Number,
    description: String,
    image: String,
    tags: [String],
    discount: {
        discountedItem: Boolean,
        discountType: String,
        discountValue: String
    },
    manuDate: Date,
    expiryDate: Date,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    reviews: [reviewSchmea],
    quantity: Number,
    status: {
        type: String,
        enum: ['available', 'sold', 'out-of-stock', 'booked'],
        default: 'available'
    }

}, {
        timestamps: true
    });

var product = mongoose.model('product', productSchema);

module.exports = product;